# VarOrg

A random organizationl change message generator. By running this code
you will get a random text that mimics the HR commnication for a new
hire.


## INSTALL

The program is composed by three bash scripts, located in the `bin`
directory. To install, just copy (or link) them into your path (e.g.
`$HOME/bin`)


## USAGE

At your prompt, type:

`makeVariazioneOrganizzativa.sh`

The language default to english (US) unless the LANG environment variable
is set to it_IT.utf-8, in that case the text will be printed in Italian.


## MISCELLANEA

You can run components individually:

* `jobTitle.sh` - creates random fictonal job titles
* `makeName.sh` - creates random unusual and unlikely Italian names

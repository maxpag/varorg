#!/usr/bin/env bash
#
# @author Massimiliano Pagani
# @version 1.0
# @date 2024-03-01
#

if [ -z "$1" ]
then
  company="ACME Unlimited"
else
  company="$1"
fi

FEMALE=0
MALE=1
declare -A PRONOUNS
PRONOUNS=(
  [en_US_0_g]=her
  [en_US_1_g]=his
  [it_IT_0_p]=lei
  [it_IT_1_p]=lui
)

lang=${LANG?-en_US.UTF-8}
lang=${lang%.*}

drawRandom()
{
  local upper=$1
  local random=$RANDOM
  echo -n $(( $random % $upper ))
}

genre=$(drawRandom 2)

newEntryJob="$(jobTitle.sh)"
newEntryName="$(makeName.sh $genre)"

bossJob="$(jobTitle.sh)"
bossName="$(makeName.sh)"

today="$(date +"%d/%m/%Y")"
future="$(( $(drawRandom 50) + 10 ))"
startDate="$(date -d "+$future days" +"%e° %B %Y")"
columns=$(tput cols)
variationCode="$(printf "%d" $(( 1+$RANDOM % 99 )) )/$(date +"%Y")"

if [ "$lang" = "it_IT" ]
then
  pronoun=${PRONOUNS[${lang}_${genre}_p]}
cat <<EOF
$company
Direzione Risorse Umane

Da: Direzione Risorse Umane
Data: $today
Oggetto: Variazione Organizzativa $variationCode
EOF

fmt --width=$columns -u << EOF
Gentili Colleghe e Colleghi,
Abbiamo il piacere di comunicarvi la seguente variazione organizzativa nella Direzione EI.
Con decorrenza $startDate, $newEntryName entrerà in ${company% *} come $newEntryJob
a riporto diretto di $bossName, $bossJob.

Diamo il benvenuto in Azienda a ${newEntryName% *} che ha già maturato una consolidata esperienza nel
settore.

A $pronoun vanno i nostri migliori auguri di soddisfazioni professionali e personali per questa nuova
sfida e responsabilità.
Con i nostri migliori saluti
EOF
else
  pronoun=${PRONOUNS[${lang}_${genre}_g]}
cat <<EOF
$company
Human Resource Department

From: Human Resource Department
Date: $today
Subject: Organizational Change $variationCode
EOF

fmt --width=$columns -u << EOF
Dear colleagues,
We are pleased to inform you of the following organizational change in the EI Department.
Starting $startDate, $newEntryName will join ${company% *} as $newEntryJob
directly reporting to $bossName, $bossJob.

We welcome ${newEntryName% *} who has already a solid experience in our sector.

Our besh wishes to $pronoun for professional and personal satisfaction for $pronoun new role

Best Regards
EOF
fi

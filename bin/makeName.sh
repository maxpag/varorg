#!/usr/bin/env bash
#
# @author Massimiliano Pagani
# @version 1.0
# @date 2024-03-01
#

set -u


FEMALE=0
MALE=1

FIRST_NAME_MALE=(
  Adenore
  Amintore
  Baldo
  Bortolo
  Calogero
  Delfino
  Doroteo
  Erminio
  Gelsomino
  Gervasino
  Gervasio
  Lucrezio
  Ottone
  Osvaldo
  Palmiro
  Pancrazio
  Remigio
  Remo
  Rinaldo
  Zeno
)

FIRST_NAME_FEMALE=(
  Addolorata
  Astolfa
  Bacheca
  Clotilde
  Clodovea
  Cleofe
  Delfina
  Dorotea
  Genoveffa
  Gertrude
  Gervasia
  Gervasina
  Iolanda
  Osvalda
  Grimilde
  Pancrazia
  Remigia
  Rinalda
  Rosalinda
  Uberta
)

LAST_NAME=(
  Auricchio
  Brigante
  Caciotta
  Gubbiotti
  Guicciardelli
  Cavalcanti
  Paccheri
  Paccheroni
  Pannacci
  Pennetta
  Ridocchi
  Ridoni
  Sparagio
  Taleggio
  Tempesta
  Zibaldoni
  Zucchini
)

NAME_PREFIXES=(
  Gian
  Pier
  Fran
)

drawRandom()
{
  local upper=$1
  local random=$RANDOM
  echo -n $(( $random % $upper ))
}

drawMaleName()
{
  local firstNameIndex=$(drawRandom ${#FIRST_NAME_MALE[@]})
  echo -n ${FIRST_NAME_MALE[$firstNameIndex]}
}

drawFemaleName()
{
  local firstNameIndex=$(drawRandom ${#FIRST_NAME_FEMALE[@]})
  echo -n ${FIRST_NAME_FEMALE[$firstNameIndex]}
}


makeFirstName()
{
  local genre="$1"
  local wantNamePrefix="$(drawRandom 5)"

  local name
  case $genre in
    $FEMALE)
      name="$(drawFemaleName)"
      ;;
    $MALE)
      name="$(drawMaleName)"
      ;;
  esac

  local namePrefix=""
  if [ $wantNamePrefix = $YES ]
  then
    local namePrefixIndex="$(drawRandom ${#NAME_PREFIXES[@]})"
    namePrefix="${NAME_PREFIXES[$namePrefixIndex]}"
    name="$(tr "A-Z" "a-z" <<< "$name")"
  fi

  local wantMiddleName="$(drawRandom 8)"  
  local middleName=""
  if [ $wantMiddleName = $YES ]
  then
    middleName=" $(makeFirstName $genre)"
  fi
  echo -n "$namePrefix$name$middleName"
}

makeName()
{
  local YES=0
  local genre=$1
  local wantNobilityPrefix="$(drawRandom 10)"
  local lastNameIndex=$(drawRandom ${#LAST_NAME[@]})

  local firstName=$(makeFirstName $genre)

  local lastName="${LAST_NAME[$lastNameIndex]}"
  local nobilityPrefix=""

  if [ $wantNobilityPrefix = $YES ]
  then
    nobilityPrefix="De "
  fi
  echo "$firstName $nobilityPrefix$lastName"
}

if [ $# = 0 ]
then
  genre="$(drawRandom 2)"
else
  case "$1" in
    1|m|M|Male|male)
      genre=$MALE
      ;;
    0|f|F|Female|female)
      genre=$FEMALE
      ;;
  esac
fi

makeName $genre

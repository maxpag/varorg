#!/usr/bin/env bash
#
# @author Massimiliano Pagani
# @version 1.0
# @date 2024-03-01
#

set -u

ROLES=(
  Archbishop
  Buyer
  Baron
  Duke
  Director
  Bishop
  CXO
  Excellence
  Evangelist
  Holiness
  Inspector
  Investigator
  Leader
  Lord
  Manager
  Priest
  Officer
  Scavenger
  Tyrant
  Supplier
)

ROLE_ATTRIBUTES=(
  "Chief"
  "Beautiful"
  "Benevolent"
  "Excellent"
  "Exquisite"
  "General"
  "Holy"
  "Principal"
  "Strategic"
  "Supreme"
  "Tactical"
  "First Class"
)

MATTERS=(
  "Buttons"
  "Cables"
  "Contracts"
  "Doors"
  "Fire Alarms"
  "Knobs"
  "Nuts and Bolts"
  "Paperwork"
  "Parts"
  "Ribbons"
  "Wires"
)

MATTER_ATTRIBUTES=(
  "Dedicated"
  "Deprecated"
  "Fake"
  "Partially-damaged"
  "Polished"
  "Refurbished"
  "Spare"
  "Special"
  "Unfinished"
  "Unusable"
  "Usable"
  "Well-crafted"
)

drawRandom()
{
  local upper=$1
  local random=$RANDOM
  echo -n $(( $random % $upper ))
}

makeTitle()
{
  local YES=0
  local wantTitleAttribute="$(drawRandom 2)"  
  local wantMatterAttribute="$(drawRandom 2)"
  local titleIndex=$(drawRandom ${#ROLES[@]})
  local matterIndex=$(drawRandom ${#MATTERS[@]})
  local title="${ROLES[$titleIndex]}"
  local matter="${MATTERS[$matterIndex]}"
  local titleAttribute=""
  local matterAttribute=""
  if [ $wantTitleAttribute = $YES ]
  then
    titleAttribute="${ROLE_ATTRIBUTES[$(drawRandom ${#ROLE_ATTRIBUTES[@]})]} "
  fi
  if [ $wantMatterAttribute = $YES ]
  then
    matterAttribute="${MATTER_ATTRIBUTES[$(drawRandom ${#MATTER_ATTRIBUTES[@]})]} " 
  fi
  if [ $(drawRandom 2) = $YES ]
  then
    echo "$titleAttribute$title of $matterAttribute$matter"
  else
    echo "$matterAttribute$matter $titleAttribute$title"
  fi
}

makeTitle
